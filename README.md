Discussion unit 5
-----------------

Implement string methods to check if there is lower case letter.

Methods implemented
-------------------

- any_lowercase1 (attempt that is not correct)
- any_lowercase2 (attempt that is not correct)
- any_lowercase3 (attempt that is not correct)
- any_lowercase4 (attempt THAT IS correct)
- any_lowercase5 (attempt that is not correct)

Test
----

type ./lo.py (to run the test)

Test environment
----------------

OS lubuntu 16.04 lts kernel version 4.13.0 python version 2.7.12
