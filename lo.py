#!/usr/bin/python

"""
FILE lo.py
"""

def any_lowercase1(s):
    """
        procedure any_lowercase1

        check if a string contains lower case letters

        this function is not correct

        it FAILS if first letter is upper case

        parameters

        s(string) initial string

        return

        check(boolean) True if first letter is lower case False if first letter is upper case
    """
    for c in s:
        if c.islower():
            return True
        else:
            return False

def any_lowercase2(s):
    """
        procedure any_lowercase2

        check if a string contains lower case letters

        this function is not correct

        it only checks for single letter c and returns 'True' in all cases becase c is lower case letter at all times

        parameters

        s(string) initial string

        return

        check(string) True if c letter is lower case False if c letter is upper case
    """
    for c in s:
        if 'c'.islower():
            return 'True'
        else:
            return 'False'

def any_lowercase3(s):
    """
        procedure any_lowercase3

        check if a string contains lower case letters

        this function is not correct

        it only checks for last occurence of lower case letter, next check changes the result

        parameters

        s(string) initial string

        return

        check(boolean) True if last letter is lower case False in other case
    """
    for c in s:
        flag = c.islower()
    return flag

def any_lowercase4(s):
    """
        procedure any_lowercase4

        check if a string contains lower case letters

        this function IS correct

        parameters

        s(string) initial string

        return

        check(boolean) True if there is lower case letter False in other case
    """
    flag = False
    for c in s:
        flag = flag or c.islower()
    return flag

def any_lowercase5(s):
    """
        procedure any_lowercase5

        check if a string contains lower case letters

        this function is not correct

        if first letter is upper case it returns False

        parameters

        s(string) initial string

        return

        check(boolean) False if first letter is upper case True in other case
    """
    for c in s:
        if not c.islower():
            return False
    return True

if __name__ == "__main__":
    stra="ABCDEFGh"
    strb="ABCDeFGH"
    strc="ABCDEFGH"
    print("---> any_lowercase1, string {0}".format(stra))
    print(any_lowercase1(stra))
    print("---> any_lowercase2, string {0} {1}".format(stra,strc))
    print(any_lowercase2(stra))
    print(any_lowercase2(strc))
    print("---> any_lowercase3, string {0}".format(strb))
    print(any_lowercase3(strb))
    print("---> any_lowercase4, strings {0} {1} {2}".format(stra,strb,strc))
    print(any_lowercase4(stra))
    print(any_lowercase4(strb))
    print(any_lowercase4(strc))
    print("---> any_lowercase5, string {0}".format(stra))
    print(any_lowercase5(stra))

"""
OUTPUT

---> any_lowercase1, string ABCDEFGh
False
---> any_lowercase2, string ABCDEFGh ABCDEFGH
True
True
---> any_lowercase3, string ABCDeFGH
False
---> any_lowercase4, strings ABCDEFGh ABCDeFGH ABCDEFGH
True
True
False
---> any_lowercase5, string ABCDEFGh
False
"""
